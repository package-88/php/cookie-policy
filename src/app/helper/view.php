<?php
	
	namespace Cpb;
	
	function view(string $path = '', array $data = []) {

		extract($data);
		ob_start();
		include($path);
		$output = ob_get_contents();
		@ob_end_clean();
		
		return $output;
	}