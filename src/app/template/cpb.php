<div id="cookie-policy-box" data-prefix="<?=$options['cookie_prefix']?>" data-main-cookie-name="cpb">
			
	<div class="align-center">
		
		<div class="padding">
			
			<div class="information-icon">
				<?=file_get_contents($icon)?>
			</div>
			
			<?php if ( ! is_null($options['main_title'])): ?>
				<span class="main-title"><?=$options['main_title']?></span>
			<?php endif; ?>
			
			<span class="main-description"><?=$options['main_description']?></span>
			
			<?php if ( ! is_null($options['main_link_label'])): ?>
				<a <?= ! is_null($options['main_link']) ? 'href="' . $options['main_link'] . '"' : ''?> class="main-link" target="_blank"><?=$options['main_link_label']?></a>
			<?php endif; ?>
			
			<?php if ( ! is_null($options['items'])): ?>
			<div class="item-list clearfix">
				
				<?php foreach ($options['items'] AS $item): ?>
				
					<?php
						$item['long_slug'] = ! is_null($options['cookie_prefix']) ? $options['cookie_prefix'] . '-' . $item['slug'] : $item['slug'];
					?>
				
					<div class="item">
						<input type="checkbox" name="<?=$item['slug']?>" id="<?=$item['long_slug']?>" data-cookie-type="<?=$item['long_slug']?>">
						<label for="<?=$item['long_slug']?>"><?=$item['label']?></label>
					</div>
				<?php endforeach; ?>
				
			</div>
			<?php endif; ?>
			
			<button class="approve-button"><?=$options['approve_label']?></button>
			
		</div>
		
	</div>
	
</div>