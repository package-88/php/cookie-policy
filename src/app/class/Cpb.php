<?php

	namespace Cpb;
	
	class Cpb
	{
		private $options = [];
		
		const ALLOWED_OPTIONS = [
			'main_title' 			=> 'optional', 
			'main_description' 		=> 'required', 
			'approve_label' 		=> 'required', 
			'main_link' 			=> 'optional',
			'main_link_label' 		=> 'optional',
			'cookie_prefix' 		=> 'optional',
			'items' 				=> 'optional'
		];
		
		public function __construct() {
			// include constants
			require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'constants.php';
			
			// include helpers
			require PATH_HELPER . 'view.php';
			
			// declare default value of options variable
			foreach (self::ALLOWED_OPTIONS AS $name => $is_required) {
				$this->options[$name] = NULL;
			}
		}
		
		public function setOption(string $name, $value) {
			
			// only allowed option can be stored
			if ( ! array_key_exists($name, self::ALLOWED_OPTIONS)) {
				trigger_error('Selected option is not allowed: \'' . $name . '\'', E_USER_NOTICE);
				return;
			}
			
			// validate item multiarray value
			if ($name == 'items') {
				
				// must be an array
				if ( ! is_array($value)) {
					trigger_error('Selected option can be set only with array: \'' . $name . '\'', E_USER_NOTICE);
					return;
				}
				
				// must have two key: label & slug
				array_map( function($a) {
					
					if ( ! array_key_exists('label', $a) || empty(trim($a['label']))) {
						trigger_error('There is a missing key in the value of item: \'label\'', E_USER_NOTICE);
						return;
					}
					
					if ( ! array_key_exists('slug', $a) || empty(trim($a['slug']))) {
						trigger_error('There is a missing key in the value of item: \'slug\'', E_USER_NOTICE);
						return;
					}
					
				}, $value);
				
			} else {
				
				// can be only string or number
				if ( ! is_string($value) && ! is_numeric($value)) {
					trigger_error('The value of the selected option can be only a string or number: \'' . $name . '\'', E_USER_NOTICE);
					return;
				}
				
				// cannot be empty
				if (empty(trim($value))) {
					trigger_error('You cannot set the selected option with empty string or null: \'' . $name . '\'', E_USER_NOTICE);
					return;
				}
				
			}
			
			$this->options[$name] = $value;
		}
		
		public function addItem(string $label, string $slug) {
			
			$value = is_null($this->options['items']) ? [] : $this->options['items'];
			$value[] = [
				'label' => $label,
				'slug' => $slug
			];
			
			$this->setOption('items', $value);
			
		}
		
		public function print(bool $save = TRUE) {
			
			// validate options property
			foreach (self::ALLOWED_OPTIONS AS $name => $is_required) {
				if ($is_required == 'required' && is_null($this->options[$name])) {
					trigger_error('There is a required option with NULL value: \'' . $name . '\'', E_USER_NOTICE);
					return;
				}
			}
			
			$output = view(PATH_TEMPLATE . 'cpb.php', [
				'options' => $this->options,
				'icon' => PATH_ICON . 'info-circle.svg'
			]);
			
			if ($save) {
				return $output;
			} else {
				echo $output;
			}
		}
		
	}
	
?>