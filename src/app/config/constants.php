<?php
	
	// define paths
	defined('PATH_ROOT') 		OR define('PATH_ROOT', realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);
	defined('PATH_SRC') 		OR define('PATH_SRC', PATH_ROOT . 'src' . DIRECTORY_SEPARATOR);
	defined('PATH_APP') 		OR define('PATH_APP', PATH_SRC . 'app' . DIRECTORY_SEPARATOR);
	defined('PATH_TEMPLATE') 	OR define('PATH_TEMPLATE', PATH_APP . 'template' . DIRECTORY_SEPARATOR);
	defined('PATH_CONFIG') 		OR define('PATH_CONFIG', PATH_APP . 'config' . DIRECTORY_SEPARATOR);
	defined('PATH_HELPER') 		OR define('PATH_HELPER', PATH_APP . 'helper' . DIRECTORY_SEPARATOR);
	defined('PATH_CLASS') 		OR define('PATH_CLASS', PATH_APP . 'class' . DIRECTORY_SEPARATOR);
	defined('PATH_ASSETS') 		OR define('PATH_ASSETS', PATH_SRC . 'assets' . DIRECTORY_SEPARATOR);
	defined('PATH_ICON') 		OR define('PATH_ICON', PATH_ASSETS . 'icon' . DIRECTORY_SEPARATOR);