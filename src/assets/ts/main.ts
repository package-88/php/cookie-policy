export class Cpb
{
	private window: Window = window;
	private document: Document = document;
	private container: HTMLDivElement;
	private approveButton: HTMLButtonElement;
	private settings: NodeListOf<HTMLInputElement>;
	
	constructor () {
		
		this.document.addEventListener('DOMContentLoaded', () => this.init() );
		
	}
	
	private init () : void {
		
		this.container = this.document.querySelector('#cookie-policy-box');
		
		if ( ! this.container) {
			return;
		}

		this.settings = this.container.querySelectorAll('.item input');
		this.approveButton = this.container.querySelector('.approve-button');
		
		this.approveButton.addEventListener('click', () => this.approve() );
		
	}
	
	private approve () : void {
		
		this.handleCookieSettings();
		this.pageRefresh();
		
	}
	
	private handleCookieSettings () : void {
		
		this.settings.forEach( item => {
			
			let cookieName = item.dataset.cookieType;
			let cookieValue = item.checked ? 'allowed' : 'disallowed';
			
			this.setCookie(cookieName, cookieValue, 30);
			
		});
		
		let mainCookieName = this.container.dataset.mainCookieName;
		this.setCookie(mainCookieName, '1', 30);
		
	}
	
	private pageRefresh () : void {
		
		this.window.location.reload(false);
		
	}
	
	private setCookie (cname: string, cvalue: string, exdays: number) : void {
		
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		
	}
	
}