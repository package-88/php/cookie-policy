// variable declaration
var browserify = require('gulp-browserify');
var path = require('path');
var fs = require('fs');
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var ts = require('gulp-typescript');
var ts_project = ts.createProject('tsconfig.json');
var browser_sync = require('browser-sync').create();
var delay_object;
//var paths = {};

// path declaration
path.root = 		'./';
path.src = 			path.root + 'src/';
path.dist = 		path.src + 'dist/';
path.assets = 		path.src + 'assets/';
path.scss = 		path.assets + 'scss/';
path.css = 			path.assets + 'css/';
path.ts = 			path.assets + 'ts/';
path.js = 			path.assets + 'js/';


// --------------------------------------------------------------------
// ---------------------------------------------- HELPER FUNCTIONS ----
// --------------------------------------------------------------------

/**
 * swallowError  
 * Log errors and continue the current job.
 * 
 * @param object error
 * @return null
 */
function swallowError (error) {
	console.log(error.toString());
	this.emit('end');
	return;
}


/**
 * convert_scss_to_css  
 * As the main logic there is only one scss file is waiting to be transpiled.
 * This main scss file contains every includes.
 * 
 * @param function
 * @return object
 */
gulp.task('convert_scss_to_css', function (cb) {
	
	// source is not exist
	if ( ! fs.existsSync(path.scss + 'main.scss')) {
		console.log('Resource is not exist: main.scss');
		return;
	}
	
	gulp.src([path.scss + 'main.scss'])
		.pipe(rename('main.css'))
		.pipe(sourcemaps.init())
		.pipe(sass()).on('error', swallowError)
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.css));
		
	cb();
});


/**
 * convert_scss_to_minimized_css  
 * Similar function as 'convert_scss_to_css', but there is a final minimizer step.
 * 
 * @param function
 * @return object
 */
gulp.task('convert_scss_to_minimized_css', function (cb) {
	
	// source is not exist
	if ( ! fs.existsSync(path.scss + 'main.scss')) {
		console.log('Resource is not exist: main.scss');
		return;
	}
	
	gulp.src(path.scss + 'main.scss')
		.pipe(rename('main.min.css'))
		.pipe(sass({outputStyle: 'compressed'})).on('error', swallowError)
		.pipe(gulp.dest(path.dist));
		
	cb();
});


/**
 * Convert typescript to javascript  
 * As the main logic there is only one typescript file is waiting to be transpiled.
 * This main typescript file contains every includes.
 * 
 * @param function
 * @return object
 */
gulp.task('convert_typescript_to_javascript', function (cb) {
	
	gulp.src(path.ts + '**/*.ts')
		.pipe(ts_project({
			removeComments: false
		}))
		.pipe(gulp.dest(path.js));
		
	cb();
});


/**
 * Bundle javascript  
 * Grab the main javascript file what contains all imports and bundle it.
 * 
 * @return null
 */
gulp.task('bundle_javascript', function (cb) {
	
	// source is not exist
	if ( ! fs.existsSync(path.js + 'main.js')) {
		console.log('Resource is not exist: main.scss');
		return;
	}
	
	gulp.src(path.js + 'main.js')
		.pipe(browserify({
			insertGlobals : true
		}))
		.pipe(rename('bundle.js'))
		.pipe(gulp.dest(path.js));
		
		gulp.src(path.js + 'example.js')
		.pipe(browserify({
			insertGlobals : true
		}))
		.pipe(rename('example.bundle.js'))
		.pipe(gulp.dest(path.js));
		
	cb();
});


/**
 * Minify and uglify javascript  
 * 
 * @return null
 */
gulp.task('minify_and_uglify_javascript', function (cb) {
	
	pump([
		gulp.src(path.js + 'main.js'),
		uglify(),
		rename('main.min.js'),
		gulp.dest(path.dist)
	]);
	
	cb();
});


/**
 * Sync Browser  
 * Send reload command to the browsers.
 * 
 * @param function callback
 * @return null
 */
gulp.task('sync_browser', function (cb) {
	
	clearTimeout(delay_object);
	
	setTimeoutObject = setTimeout(function() {
		browser_sync.reload();
	}, 400);
	
	cb();
});



// --------------------------------------------------------------
// ---------------------------------------------- MAIN TASKS ----
// --------------------------------------------------------------

gulp.task('dev', gulp.parallel(
	'convert_scss_to_css',
	gulp.series('convert_typescript_to_javascript', 'bundle_javascript'),
	function (cb) {
		
		browser_sync.init({
			proxy: 'package.webdev'
		});
		
		gulp.watch(path.scss + '**/*.scss', gulp.series('convert_scss_to_css', 'sync_browser'));
		gulp.watch(path.ts + '**/*.ts', gulp.series('convert_typescript_to_javascript', 'bundle_javascript', 'sync_browser'));
		
		cb();
	}
));

gulp.task('prod', gulp.parallel(
	'convert_scss_to_minimized_css',
	gulp.series('convert_typescript_to_javascript', 'minify_and_uglify_javascript')
));