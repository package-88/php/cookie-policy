<?php
	
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	use Cpb\Cpb;

	if ( ! array_key_exists('cpb', $_COOKIE)) {

		require './../src/app/class/Cpb.php';
		
		
		$cpb = new Cpb();
		
		$cpb->setOption('main_title', 'Sütikezelés');
		$cpb->setOption('main_description', 'Weboldalunk az alapvető működéshez szükséges cookie-kat használ. Szélesebb körű fukcionalitáshoz marketing jellegű cookiekat engedélyezhet, amivel elfogadja az Adatvédelmi tájékoztatóban foglaltakat.');
		$cpb->setOption('main_link', 'https://joepetersburger.com');
		$cpb->setOption('main_link_label', 'További információ');
		$cpb->setOption('approve_label', 'Elfogadom');
		$cpb->setOption('items', [
			['label' => 'Működéshez szükséges cookie-k', 'slug' => 'mukodes'],
			['label' => 'Statisztikai cookie-k', 'slug' => 'statisztika'],
			['label' => 'Beállítás cookie-k', 'slug' => 'beallitas']
			]);
		$cpb->setOption('cookie_prefix', 'cpb');

	}
	
?>

<!DOCTYPE html>
<html>
	<head>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
		<meta charset="UTF-8" />
		<title>Example of Cookie Policy</title>
		<link rel="stylesheet" type="text/css" href="./../src/assets/css/main.css">
		<script src="./../src/assets/js/example.bundle.js"></script>
	</head>
	<body>
		<h1>Cookie Policy example</h1>
		
		<!-- cookie policy box -->
		<?php if ( ! array_key_exists('cpb', $_COOKIE)): ?>
			<?=$cpb->print()?>
		<?php endif; ?>
		<!-- /cookie policy box -->
		
	</body>
</html>